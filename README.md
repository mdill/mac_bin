# Mac OSX binary files

## Purpose

This repository consists of OSX binary files, which are commonly found in Linux,
but are not available in Apple's OSX.  These are designed to make the user's
life easier, and streamline changes between platforms

## Downloading

    cd ~/
    git clone https:/bitbucket.org/mdill/mac_bin.git

## Installation

Once cloned, the Makefile can be initiated, simply by using the `make` command.
This will copy the required files to the user's `/usr/local/bin` directory and
make them executable.

## Use

If implemented correctly, the Makefile will make each executable program
available to your shell.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/mac_bin/src/8595d970e28d811179fc12fd70dfd629f7353d76/LICENSE.txt?at=master) file for
details.

