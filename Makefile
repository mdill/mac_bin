#
# `make`			Build executable file in the /usr/local/bin directory.
# `make clean`		Removes the executable file
#
# *****************************************************************************

# Source files
#
#
SRCS = {showHidden,hideHidden,pidof}
CURRDIR = $(shell pwd)
OBJDIR = /usr/local/bin

# *****************************************************************************

all: stuffs

# Copy the binaries, then make them executable
#
stuffs: 
	for f in $(SRCS); do \
		sudo cp $$f $(OBJDIR); \
		sudo chmod +x $(OBJDIR)/$$f; \
	done

# Remove the executable, just for insurance
#
clean:
	for f in $(SRCS); do \
		sudo rm $(OBJDIR)/$$f; \
	done

